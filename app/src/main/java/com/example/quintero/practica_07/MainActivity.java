package com.example.quintero.practica_07;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

   EditText men,num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }



    public void makeCall(View v) {
        Toast t;
        num = (EditText) findViewById(R.id.num);

        if(! num.getText().toString().trim().equals("")){

            try {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+num.getText().toString()));
                try {
                    startActivity(callIntent);
                } catch (ActivityNotFoundException e) {
                    t = Toast.makeText(getApplicationContext(), "Call Falló: " + e, Toast.LENGTH_SHORT);
                    t.show();
                }
            } catch (ActivityNotFoundException activityException) {
                Log.e("dialing-example", "Call failed", activityException);
            }
        }
    }




    public void onClick(View v) {
        num = (EditText) findViewById(R.id.num);
        men = (EditText) findViewById(R.id.txtmensaje);
        Toast t;
        if(! men.getText().toString().trim().equals("") && !num.getText().toString().trim().equals("")) {
            sendSMS(num.getText().toString(), men.getText().toString());
            t = Toast.makeText(getApplicationContext(), "Mensaje Enviado", Toast.LENGTH_SHORT);
            t.show();
        }else{
            t = Toast.makeText(getApplicationContext(), "Escribe un Numero/Mensaje ", Toast.LENGTH_SHORT);
            t.show();
        }
    }
    //---sends an SMS message to another device---
    private void sendSMS(String phoneNumber, String message)
    {
        SmsManager sms = SmsManager.getDefault();  // solo esto es necesario
        EditText n =(EditText) findViewById(R.id.cantidad);
        if(!n.getText().toString().trim().equals("")){

            String j = n.getText().toString();
            int k = Integer.parseInt(j);
            for (int i=0; i<k; i++){
                sms.sendTextMessage(phoneNumber, null, message, null, null);
            }
        }
        else{
            sms.sendTextMessage(phoneNumber, null, message, null, null); // y esto tambien
        }


    }
}
